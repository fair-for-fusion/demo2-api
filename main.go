package main

import (
	// clientV1alpha1 "github.com/martin-helmich/kubernetes-crd-example/clientset/v1alpha1"
	// metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"io/ioutil"

	oidc "github.com/coreos/go-oidc"
	"github.com/gorilla/mux"
	"golang.org/x/oauth2"
	"gopkg.in/yaml.v2"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	api "rest.f4f.com/api/types/v1alpha1"
)

type authentication_verification struct {
	verifier     *oidc.IDTokenVerifier
	oauth2Config *oauth2.Config
	state        string
	context      context.Context
}

var auth = &authentication_verification{}

func displayPage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage!")
	fmt.Println("Endpoint Hit: homePage")
}

func getStatusAll(w http.ResponseWriter, r *http.Request) {
	// Extract custom claims
	var claims struct {
		Groups []string `json:"f4f-groups"`
	}

	rawAccessToken := r.Header.Get("Authorization")
	if rawAccessToken == "" {
		w.Write([]byte("Invalid access token"))
		return
	}
	fmt.Println(rawAccessToken)
	parts := strings.Split(rawAccessToken, " ")
	if len(parts) != 2 {
		w.WriteHeader(400)
		return
	}
	accessToken, err := auth.verifier.Verify(auth.context, parts[1])
	if err != nil {
		w.Write([]byte("Invalid access token"))
		return
	}

	if err := accessToken.Claims(&claims); err != nil {
		fmt.Println(err.Error())
		return
	}

	found := 0
	for _, group := range claims.Groups {
		if group == "fair4fusion/pipelines" {
			found = 1
			break
		}
	}
	if found == 0 {
		w.Write([]byte("No access to group"))
		return
	}
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err)
	}
	clientset, _ := kubernetes.NewForConfig(config)
	// access the API to list pods
	pods, _ := clientset.CoreV1().Pods("default").List(v1.ListOptions{})
	for _, pod := range pods.Items {
		json.NewEncoder(w).Encode(pod)
	}

	fmt.Printf("There are %d pods in the cluster\n", len(pods.Items))
}

func getStatus(w http.ResponseWriter, r *http.Request) {
	// Extract custom claims
	var claims struct {
		Groups []string `json:"f4f-groups"`
	}

	rawAccessToken := r.Header.Get("Authorization")
	if rawAccessToken == "" {
		w.Write([]byte("Invalid access token"))
		return
	}
	fmt.Println(rawAccessToken)
	parts := strings.Split(rawAccessToken, " ")
	if len(parts) != 2 {
		w.WriteHeader(400)
		return
	}
	accessToken, err := auth.verifier.Verify(auth.context, parts[1])
	if err != nil {
		w.Write([]byte("Invalid access token"))
		return
	}

	if err := accessToken.Claims(&claims); err != nil {
		fmt.Println(err.Error())
		return
	}

	found := 0
	for _, group := range claims.Groups {
		if group == "fair4fusion/pipelines" {
			found = 1
			break
		}
	}
	if found == 0 {
		w.Write([]byte("No access to group"))
		return
	}

	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err)
	}
	clientset, _ := kubernetes.NewForConfig(config)
	// access the API to list pods
	pods, _ := clientset.CoreV1().Pods("default").List(v1.ListOptions{LabelSelector: "my_label=f4f-operator"})
	for _, pod := range pods.Items {
		json.NewEncoder(w).Encode(pod)
	}

	fmt.Printf("There are %d pods in the cluster\n", len(pods.Items))
}

func fetch(w http.ResponseWriter, r *http.Request) {
	// Extract custom claims
	var claims struct {
		Groups []string `json:"f4f-groups"`
	}

	rawAccessToken := r.Header.Get("Authorization")
	if rawAccessToken == "" {
		w.Write([]byte("Invalid access token"))
		return
	}
	fmt.Println(rawAccessToken)
	parts := strings.Split(rawAccessToken, " ")
	if len(parts) != 2 {
		w.WriteHeader(400)
		return
	}
	accessToken, err := auth.verifier.Verify(auth.context, parts[1])
	if err != nil {
		w.Write([]byte("Invalid access token"))
		return
	}

	if err := accessToken.Claims(&claims); err != nil {
		fmt.Println(err.Error())
		return
	}

	found := 0
	for _, group := range claims.Groups {
		if group == "fair4fusion/pipelines" {
			found = 1
			break
		}
	}
	if found == 0 {
		w.Write([]byte("No access to group"))
		return
	}

	path := r.URL.Query().Get("arg")

	sourceFile := "/shared/" + path

	input, err := ioutil.ReadFile(sourceFile)
	if err != nil {
		fmt.Println(err)
		return
	}
	w.Write([]byte(input))

}

func show(w http.ResponseWriter, r *http.Request) {
	// Extract custom claims
	var claims struct {
		Groups []string `json:"f4f-groups"`
	}

	rawAccessToken := r.Header.Get("Authorization")
	if rawAccessToken == "" {
		w.Write([]byte("Invalid access token"))
		return
	}
	fmt.Println(rawAccessToken)
	parts := strings.Split(rawAccessToken, " ")
	if len(parts) != 2 {
		w.WriteHeader(400)
		return
	}
	accessToken, err := auth.verifier.Verify(auth.context, parts[1])
	if err != nil {
		w.Write([]byte("Invalid access token"))
		return
	}

	if err := accessToken.Claims(&claims); err != nil {
		fmt.Println(err.Error())
		return
	}

	found := 0
	for _, group := range claims.Groups {
		if group == "fair4fusion/pipelines" {
			found = 1
			break
		}
	}
	if found == 0 {
		w.Write([]byte("No access to group"))
		return
	}

	path := "/shared/" + r.URL.Query().Get("arg")

	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		w.Write([]byte(f.Name()))
		w.Write([]byte("\n"))
	}

}

func addQuery(w http.ResponseWriter, r *http.Request) {

	// Extract custom claims
	var claims struct {
		Groups []string `json:"f4f-groups"`
	}

	rawAccessToken := r.Header.Get("Authorization")
	if rawAccessToken == "" {
		w.Write([]byte("Invalid access token"))
		return
	}
	fmt.Println(rawAccessToken)
	parts := strings.Split(rawAccessToken, " ")
	if len(parts) != 2 {
		w.WriteHeader(400)
		return
	}
	accessToken, err := auth.verifier.Verify(auth.context, parts[1])
	if err != nil {
		w.Write([]byte("Invalid access token"))
		return
	}

	if err := accessToken.Claims(&claims); err != nil {
		fmt.Println(err.Error())
		return
	}

	found := 0
	for _, group := range claims.Groups {
		if group == "fair4fusion/pipelines" {
			found = 1
			break
		}
	}
	if found == 0 {
		w.Write([]byte("No access to group"))
		return
	}

	r.ParseMultipartForm(10 << 20)

	file, handler, err := r.FormFile("myFile")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	destinationFile := "/shared/" + handler.Filename
	err = ioutil.WriteFile(destinationFile, fileBytes, 0644)
	if err != nil {
		fmt.Println("Error creating", destinationFile)
		fmt.Println(err)
		return
	}
	fmt.Fprintf(w, "Successfully Uploaded File\n")
}

func deployExperiment(w http.ResponseWriter, r *http.Request) {

	// Extract custom claims
	var claims struct {
		Groups []string `json:"f4f-groups"`
	}

	rawAccessToken := r.Header.Get("Authorization")
	if rawAccessToken == "" {
		w.Write([]byte("Invalid access token"))
		return
	}
	fmt.Println(rawAccessToken)
	parts := strings.Split(rawAccessToken, " ")
	if len(parts) != 2 {
		w.WriteHeader(400)
		return
	}
	accessToken, err := auth.verifier.Verify(auth.context, parts[1])
	if err != nil {
		w.Write([]byte("Invalid access token"))
		return
	}

	if err := accessToken.Claims(&claims); err != nil {
		fmt.Println(err.Error())
		return
	}

	found := 0
	for _, group := range claims.Groups {
		if group == "fair4fusion/pipelines" {
			found = 1
			break
		}
	}
	if found == 0 {
		w.Write([]byte("No access to group"))
		return
	}

	fmt.Println("File Upload Endpoint Hit")

	r.ParseMultipartForm(10 << 20)

	file, handler, err := r.FormFile("myFile")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	destinationFile := "/shared/experiments/" + handler.Filename
	err = ioutil.WriteFile(destinationFile, fileBytes, 0644)
	if err != nil {
		fmt.Println("Error creating", destinationFile)
		fmt.Println(err)
		return
	}

	c := &api.F4F{}
	err = yaml.Unmarshal(fileBytes, c)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}

	fmt.Fprintf(w, "Successfully Uploaded File\n")

	exp := newExperiment(c)
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err)
	}

	api.AddToScheme(scheme.Scheme)

	crdConfig := *config

	crdConfig.ContentConfig.GroupVersion = &schema.GroupVersion{Group: api.GroupName, Version: api.GroupVersion}
	crdConfig.APIPath = "/apis"
	crdConfig.NegotiatedSerializer = scheme.Codecs.WithoutConversion()
	crdConfig.UserAgent = rest.DefaultKubernetesUserAgent()

	exampleRestClient, err := rest.UnversionedRESTClientFor(&crdConfig)
	if err != nil {
		panic(err)
	}

	result := api.F4F{}
	if err = exampleRestClient.Post().Namespace("default").Resource("f4fs").Body(exp).Do().Into(&result); err != nil {

		fmt.Printf("Unable to create F4F experiment: %+v\n", err)

	} else {
		fmt.Printf("F4F experiment created: %+v\n", result)

	}
}

func callback(w http.ResponseWriter, r *http.Request) {
	if r.URL.Query().Get("state") != auth.state {
		http.Error(w, "state did not match", http.StatusBadRequest)
		return
	}

	oauth2Token, err := auth.oauth2Config.Exchange(auth.context, r.URL.Query().Get("code"))
	if err != nil {
		http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)
		return
	}
	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		http.Error(w, "No id_token field in oauth2 token.", http.StatusInternalServerError)
		return
	}
	idToken, err := auth.verifier.Verify(auth.context, rawIDToken)
	if err != nil {
		http.Error(w, "Failed to verify ID Token: "+err.Error(), http.StatusInternalServerError)
		return
	}

	resp := struct {
		OAuth2Token   *oauth2.Token
		IDTokenClaims *json.RawMessage // ID Token payload is just JSON.
	}{oauth2Token, new(json.RawMessage)}

	if err := idToken.Claims(&resp.IDTokenClaims); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	data, err := json.MarshalIndent(resp, "", "    ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(data)

}

func getToken(w http.ResponseWriter, r *http.Request) {

	// Extract custom claims
	var claims struct {
		Groups []string `json:"f4f-groups"`
	}

	rawAccessToken := r.Header.Get("Authorization")
	if rawAccessToken == "" {
		http.Redirect(w, r, auth.oauth2Config.AuthCodeURL(auth.state), http.StatusFound)
		return
	}
	fmt.Println(rawAccessToken)
	parts := strings.Split(rawAccessToken, " ")
	if len(parts) != 2 {
		w.WriteHeader(400)
		return
	}
	_, err := auth.verifier.Verify(auth.context, parts[1])
	if err != nil {
		http.Redirect(w, r, auth.oauth2Config.AuthCodeURL(auth.state), http.StatusFound)
		fmt.Println(err.Error())
		return
	}
	accessToken, _ := auth.verifier.Verify(auth.context, parts[1])

	if err := accessToken.Claims(&claims); err != nil {
		fmt.Println(err.Error())

	}

	w.Write([]byte("hello world"))
}

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/get_token", getToken)
	myRouter.HandleFunc("/callback", callback)
	myRouter.HandleFunc("/display", displayPage)
	myRouter.HandleFunc("/status/", getStatus)
	myRouter.HandleFunc("/show", show)
	myRouter.HandleFunc("/status_all/", getStatusAll)
	myRouter.HandleFunc("/fetch", fetch).Methods("GET")
	myRouter.HandleFunc("/deploy", deployExperiment).Methods("POST")
	myRouter.HandleFunc("/addQuery", addQuery).Methods("POST")

	log.Fatal(http.ListenAndServe(":8000", myRouter))
}

func newExperiment(exp *api.F4F) *api.F4F {

	experiment := &api.F4F{

		ObjectMeta: exp.ObjectMeta,
		Spec:       exp.Spec,
	}
	return experiment
}

func main() {

	configURL := "http://accounts.fair4fusion.iit.demokritos.gr/auth/realms/f4f"
	ctx := context.Background()
	provider, err := oidc.NewProvider(ctx, configURL)
	if err != nil {
		panic(err)
	}
	//oidc.NewRemoteKeySet()
	clientID := "f4f-rest-op-api"
	clientSecret := "32ece210-056a-48d5-bbab-46efb7dd70ba"
	redirectURL := "http://rest-operator.fair4fusion.iit.demokritos.gr/callback"
	// Configure an OpenID Connect aware OAuth2 client.
	oauth2Config := oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		RedirectURL:  redirectURL,
		// Discovery returns the OAuth2 endpoints.
		Endpoint: provider.Endpoint(),
		// "openid" is a required scope for OpenID Connect flows.
		Scopes: []string{oidc.ScopeOpenID, "profile", "email"},
	}
	state := "somestate"

	oidcConfig := &oidc.Config{
		ClientID:          clientID,
		SkipClientIDCheck: true,
	}

	verifier := provider.Verifier(oidcConfig)

	auth = &authentication_verification{verifier: verifier, oauth2Config: &oauth2Config, state: state, context: ctx}

	handleRequests()

}
